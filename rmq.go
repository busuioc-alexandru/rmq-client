package rmq

import (
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"math/rand"
	"runtime"
	"sync"
	"time"
)

type QueueListenerCallback func(message amqp.Delivery)

// Client holds necessary information for rabbitMQ
type Client struct {
	connection      *amqp.Connection
	channel         *amqp.Channel
	queueName       string
	listenCallback  QueueListenerCallback
	logger          *log.Logger
	done            chan bool
	notifyTerminate chan bool
	notifyClose     chan *amqp.Error
	notifyConfirm   chan amqp.Confirmation
	isConnected     bool
	streamWip       *sync.WaitGroup
}

const (
	// When reconnecting to the server after connection failure
	reconnectDelay = 5 * time.Second

	// When resending messages the server didn't confirm
	resendDelay = 5 * time.Second

	// When resending messages the server didn't confirm
	retrySendDelay = 200 * time.Millisecond

	PushContentTypeTextPlain       = "text/plain"
	PushContentTypeApplicationJson = "application/json"
)

var (
	ErrDisconnected  = errors.New("disconnected from rabbitmq, trying to reconnect")
	errNotConnected  = errors.New("not connected to the queue")
	errAlreadyClosed = errors.New("already closed: not connected to the queue")
	consumerId       int
)

func init() {
	rand.Seed(time.Now().UnixNano())
	consumerId = rand.Int()
}

// New is a constructor that takes address, push and listen queue names, logger, and a channel that
// will notify rabbitmq client on server shutdown. We calculate the number of threads, create the client,
// and start the connection process. Connect method connects to the rabbitmq server and
// creates push/listen channels if they don't exist.
func New(queueName, addr string, cb QueueListenerCallback, logger *log.Logger) *Client {

	client := Client{
		logger:          logger,
		queueName:       queueName,
		listenCallback:  cb,
		done:            make(chan bool),
		notifyTerminate: make(chan bool, 1),
		streamWip:       &sync.WaitGroup{},
	}

	go client.handleReconnect(addr)
	return &client
}

// handleReconnect will wait for a connection error on
// notifyClose, and then continuously attempt to reconnect.
func (c *Client) handleReconnect(addr string) {
	for {
		c.isConnected = false
		c.logger.Printf("(i) attempting to connect to RabbitMQ: %s\n", addr)
		start := time.Now()
		for !c.connect(addr) {
			c.logger.Printf("(!) failed to connect. Retrying in %s...", reconnectDelay.String())
			time.Sleep(reconnectDelay)
		}
		c.logger.Printf("(i) connected to rabbitMQ in %s", time.Since(start).String())
		select {
		case <-c.done:
			c.logger.Print("the client is closing; skip connect retry")
			return
		case <-c.notifyClose:
		}
	}
}

// connect will make a single attempt to connect to
// RabbitMQ. It returns the success of the attempt.
func (c *Client) connect(addr string) bool {
	conn, err := amqp.Dial(addr)
	if err != nil {
		c.logger.Printf("failed to dial RabbitMQ server: %v", err)
		return false
	}
	ch, err := conn.Channel()
	if err != nil {
		c.logger.Printf("failed connecting to channel: %v", err)
		return false
	}

	_ = ch.Confirm(false)

	_, err = ch.QueueDeclare(
		c.queueName,
		true,  // Durable
		false, // Delete when unused
		false, // Exclusive
		false, // No-wait
		nil,   // Arguments
	)
	if err != nil {
		c.logger.Printf("failed to declare [%s] queue: %v", c.queueName, err)
		return false
	}

	c.changeConnection(conn, ch)
	c.isConnected = true
	return true
}

// changeConnection takes a new connection to the queue,
// and updates the channel listeners to reflect this.
func (c *Client) changeConnection(connection *amqp.Connection, channel *amqp.Channel) {
	c.connection = connection
	c.channel = channel
	c.notifyClose = c.channel.NotifyClose(make(chan *amqp.Error, 1))
	c.notifyConfirm = c.channel.NotifyPublish(make(chan amqp.Confirmation, 1))
}

// Push will push data onto the queue, and wait for a confirmation.
// If no confirms are received until within the resendTimeout,
// it continuously resends messages until a confirmation is received.
// This will block until the server sends a confirm. Errors are
// only returned if the push action itself fails, see UnsafePush.
func (c *Client) Push(data []byte, contentType string) error {
	for {
		err := c.UnsafePush(data, contentType)
		if err != nil {
			if err == errNotConnected {
				c.logger.Println(fmt.Errorf("push failed. retrying in %s", retrySendDelay.String()))
				time.Sleep(retrySendDelay)
			} else {
				c.logger.Println(fmt.Errorf("push failed. retrying now"))
			}
			continue
		}
		select {
		case confirm := <-c.notifyConfirm:
			if confirm.Ack {
				// c.logger.Println("push confirmed with tag", confirm.DeliveryTag)
				return nil
			}
		case <-time.After(resendDelay):
			c.logger.Println(fmt.Errorf("push didn't confirm. retrying now"))
			continue
		}
	}
}

// UnsafePush will push to the queue without checking for
// confirmation. It returns an error if it fails to connect.
// No guarantees are provided for whether the server will
// receive the message.
func (c *Client) UnsafePush(data []byte, contentType string) error {
	if !c.isConnected {
		return errNotConnected
	}
	return c.channel.Publish(
		"",          // Exchange
		c.queueName, // Routing key
		false,       // Mandatory
		false,       // Immediate
		amqp.Publishing{
			ContentType: contentType,
			Body:        data,
		},
	)
}

// Stream will continuously put queue items on the channel.
// It is required to call delivery.Ack when it has been
// successfully processed, or delivery.Nack when it fails.
// Ignoring this will cause data to build up on the server.
func (c *Client) Stream() error {
	for {
		if c.isConnected {
			break
		}
		time.Sleep(10 * time.Millisecond)
	}

	err := c.channel.Qos(runtime.NumCPU()*80, 0, false)
	if err != nil {
		return err
	}

	var connectionDropped bool

	c.streamWip.Add(1)
	consumerName := consumerName()
	msgs, err := c.channel.Consume(
		c.queueName,
		consumerName, // Consumer
		false,        // Auto-Ack
		false,        // Exclusive
		false,        // No-local
		false,        // No-Wait
		nil,          // Args
	)
	if err != nil {
		c.streamWip.Done()
		return err
	}

	go func() {
		defer c.streamWip.Done()
		for {
			select {
			case <-c.done:
				log.Println(" > [client].done for consumer " + consumerName)
				return
			case msg, ok := <-msgs:
				if !ok {
					connectionDropped = true
					return
				}
				c.parseEvent(msg)
			}
		}
	}()

	c.streamWip.Wait()

	if connectionDropped {
		return ErrDisconnected
	}

	return nil
}

func (c *Client) parseEvent(msg amqp.Delivery) {
	logger := c.logger

	defer func(m amqp.Delivery, logger *log.Logger) {
		if err := recover(); err != nil {
			stack := make([]byte, 8096)
			stack = stack[:runtime.Stack(stack, false)]
			logger.Println(" > panic recovery for rabbitMQ message", stack)
			_ = msg.Nack(false, false)
		}
	}(msg, logger)

	c.listenCallback(msg)
}

// Close will cleanly shutdown the channel and connection.
func (c *Client) Close() error {
	if !c.isConnected {
		return errAlreadyClosed
	}
	// stop receiving new messages
	consumerName := consumerName()
	c.logger.Printf("stop receiving for consumer %s", consumerName)
	err := c.channel.Cancel(consumerName, false)
	if err != nil {
		return fmt.Errorf("error canceling consumer %s: %v", consumerName, err)
	}
	// mark the client as closed
	close(c.done)
	// wait for the consumers to return
	c.logger.Println("Waiting for the received messages to be processed...")
	c.streamWip.Wait()
	// close the channel and the connection
	err = c.channel.Close()
	if err != nil {
		return err
	}
	err = c.connection.Close()
	if err != nil {
		return err
	}
	c.isConnected = false
	c.logger.Println("gracefully stopped rabbitMQ connection")
	return nil
}

func consumerName() string {
	return fmt.Sprintf("go-consumer-%v", consumerId)
}
