package rmq

import (
	"github.com/streadway/amqp"
	"log"
	"os"
	"sync"
	"testing"
	"time"
)

func TestLib_Publish(t *testing.T) {
	conn, _ := amqp.Dial("amqp://guest:guest@localhost:5672")
	channel, _ := conn.Channel()
	q, _ := channel.QueueDeclare("q", false, false, false, false, nil)
	_ = channel.Confirm(false)
	// closeS := make(chan *amqp.Error)
	confirm := channel.NotifyPublish(make(chan amqp.Confirmation, 2))
	wg := sync.WaitGroup{}
	for i := 0; i <= 100; i++ {
		_ = channel.Publish(
			"",
			q.Name,
			false,
			false,
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte("hello"),
			})
		log.Println("Published")
		go func(wg *sync.WaitGroup) {
			wg.Add(1)
			defer wg.Done()
			c := <-confirm

			log.Println("confirmed", c.DeliveryTag)
		}(&wg)
	}
	wg.Wait() // for reference see @link https://github.com/streadway/amqp/issues/254
}

func TestClient_Push(t *testing.T) {
	client := New("test", "amqp://guest:guest@localhost:5672", nil, log.New(os.Stderr, "", log.LstdFlags))
	time.Sleep(time.Second)
	for i := 0; i <= 100; i++ {
		_ = client.Push([]byte("hello"), "text/plain")
		log.Println("Published")
	}
}
